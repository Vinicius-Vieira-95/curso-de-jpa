package br.kerberossec.application;

import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.kerberossec.entities.Cliente;
import br.kerberossec.service.ClienteService;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Clientes-PU");
		ClienteService service = new ClienteService(emf);
		Scanner sc = new Scanner(System.in);
		Cliente cliente = null;
		List<Cliente> clientes = null;
		String cpf, nome;
		Integer idade;
		
		System.out.println("Bem-vindo ao sistema de gerenciamento de Clientes!");
		System.out.println("\tCriado por Francisco Robson de Oliveira Dutra Filho");
		System.out.println("\tDiscent de Ciência da Computação - UECE");
		System.out.println("\tData de criação: 26/11/2022");
		System.out.println();
		
		do {
			System.out.println("Menu de seleção:");
			System.out.println("\t1. Listar clientes");
			System.out.println("\t2. Encontrar cliente");
			System.out.println("\t3. Adicionar cliente");
			System.out.println("\t4. Atualizar cliente");
			System.out.println("\t5. Remover cliente");
			
			System.out.print("Escolha um número do menu: ");
			int comando = sc.nextInt();
			
			switch(comando) {
				case 1:
					int pagina=1;
					String comandoPagina;
					do {
						clientes = service.listarClientesComPaginacao(pagina);
						System.out.println("LISTANDO CLIENTES...");
						for(Cliente c : clientes) {
							System.out.println("CLIENTE: " + c.getNome() + ", "
									+ c.getIdade() + " anos, "
									+ c.getCpf());
						}
						System.out.println("\tPágina: " + pagina);
						System.out.println("Caso queira sair deste menu, insira qualquer conteúdo abaixo.");
						System.out.print("[proximo/anterior]: ");
						
						comandoPagina = sc.next();
						
						if(comandoPagina.toLowerCase().equals("proximo")) {
							pagina++;
						} else if(comandoPagina.toLowerCase().equals("anterior") && pagina > 1) {
							pagina--;
						} else {
							break;
						}
					} while(true);
					break;
				case 2:
					System.out.println("ENCONTRAR CLIENTE...");
					System.out.print("Insira o CPF do cliente: ");
					cpf = sc.next();
					cliente = service.pegarClientePorCPF(cpf);
					if(cliente != null) {
						System.out.println("CLIENTE ENCONTRADO!");
						System.out.println("CLIENTE: " + cliente.getNome() + ", "
								+ cliente.getIdade() + " anos, "
								+ cliente.getCpf());
					}
					break;
				case 3:
					cliente = new Cliente();
					System.out.println("ADICIONAR CLIENTE...");
					System.out.print("Insira o CPF do cliente: ");
					cpf = sc.next();
					System.out.print("Insira o nome do cliente: ");
					nome = sc.next();
					System.out.print("Insira a idade do cliente: ");
					idade = sc.nextInt();
					
					cliente.setCpf(cpf);
					cliente.setNome(nome);
					cliente.setIdade(idade);
					
					System.out.println("ADICIONANDO CLIENTE...");
					service.adicionarCliente(cliente);
					System.out.println("CLIENTE ADICIONADO!");
					break;
				case 4:
					cliente = new Cliente();
					System.out.println("ATUALIZAR CLIENTE...");
					System.out.print("Insira o CPF do cliente: ");
					cpf = sc.next();
					System.out.print("Insira o nome do cliente: ");
					nome = sc.next();
					System.out.print("Insira a idade do cliente: ");
					idade = sc.nextInt();
					
					cliente.setCpf(cpf);
					cliente.setNome(nome);
					cliente.setIdade(idade);
					
					service.atualizarCliente(cliente);
					
					System.out.println("ATUALIZANDO CLIENTE...");
					System.out.println("VERIFICANDO SE O CLIENTE FOI ATUALIZADO...");
					if(service.pegarClientePorCPF(cliente.getCpf()) != null) {
						System.out.println("CLIENTE ATUALIZADO!");
					} else {
						System.out.println("ACONTECEU ALGUM ERRO AO ATUALIZAR, TENTE NOVAMENTE...");
					}
					break;
				case 5:
					System.out.println("REMOVER CLIENTE...");
					System.out.print("Insira o CPF do cliente: ");
					cpf = sc.next();
					
					System.out.println("REMOVENDO CLIENTE...");
					service.removerCliente(cpf);
					System.out.println("REQUISIÇÃO PROCESSADA!");
					break;
				default:
					System.out.println("Comando inválido! Por favor, insira um número no intervalo de 1 até 5.");
					break;
			}
			
			System.out.print("Deseja continuar? [S/n] ");
			String comandoContinuar = sc.next();
			
			if(comandoContinuar.toLowerCase().equals("n")) {
				break;
			}
			
		} while(true);
		
		System.out.println("Obrigado por testar meu programa! :D");
		
		service.close();
		sc.close();
		emf.close();
	}

}
