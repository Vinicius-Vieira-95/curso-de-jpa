package com.cursojpa.aula1.cursojpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.cursojpa.aula1.cursojpa.entities.Cliente;

public class Program {

	public static void main(String[] args) {
		
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("Clientes-PU");
		
		EntityManager em = emf.createEntityManager();
		
		Cliente c1 = new Cliente(1, "Maria");
		Cliente c2 = new Cliente(2, "João");
		Cliente c3 = new Cliente(3, "Marcos");
		
		/*
		em.getTransaction().begin();
		em.persist(c1);
		em.persist(c2);
		em.persist(c3);
		em.getTransaction().commit();
		*/
		Cliente query1 = em.find(Cliente.class, 1);
		Cliente query2 = em.find(Cliente.class, 2);
		Cliente query3 = em.find(Cliente.class, 3);
		
		System.out.println(query1.getName()+", "+ query1.getId());
		System.out.println(query2.getName()+", "+ query2.getId());
		System.out.println(query3.getName()+", "+ query3.getId());
		
		
		em.close();
		emf.close();

	}

}
