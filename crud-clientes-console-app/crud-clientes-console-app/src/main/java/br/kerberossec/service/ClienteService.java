package br.kerberossec.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.kerberossec.entities.Cliente;

public class ClienteService {
	
	private EntityManagerFactory emf;
	
	public ClienteService(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	public List<Cliente> listarClientesComPaginacao(int pagina) {
		EntityManager em = emf.createEntityManager();
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Cliente> criteriaQuery = criteriaBuilder.createQuery(Cliente.class);
		Root<Cliente> root = criteriaQuery.from(Cliente.class);
		
		criteriaQuery.select(root);
		
		TypedQuery<Cliente> typedQuery = em.createQuery(criteriaQuery)
				.setMaxResults(10)
				.setFirstResult(0 + ((pagina * 10) - 10));
		List<Cliente> clientes = typedQuery.getResultList();
		
		em.close();
		return clientes;
	}
	
	public Cliente pegarClientePorCPF(String cpf) {
		EntityManager em = emf.createEntityManager();
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Cliente> criteriaQuery = criteriaBuilder.createQuery(Cliente.class);
		Root<Cliente> root = criteriaQuery.from(Cliente.class);
		try {
			criteriaQuery.select(root);
			criteriaQuery.where(criteriaBuilder.equal(root.get("cpf"), cpf));
		
			TypedQuery<Cliente> typedQuery = em.createQuery(criteriaQuery);
			Cliente cliente = typedQuery.getSingleResult();
			return cliente;
		} catch(NoResultException e) {
			System.out.println("Cliente não encontrado!");
		} catch(Exception e) {
			System.out.println("Aconteceu algum erro inesperado no servidor!");
		}
		em.close();
		return null;
	}
	
	public void adicionarCliente(Cliente cliente) {
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		em.persist(cliente);
		em.getTransaction().commit();
		
		em.close();
	}
	
	public void removerCliente(String cpf) {
		EntityManager em = emf.createEntityManager();
		
		Cliente cliente = this.pegarClientePorCPF(cpf);
		if(cliente != null) {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaDelete<Cliente> criteriaDelete = criteriaBuilder.createCriteriaDelete(Cliente.class);
			Root<Cliente> root = criteriaDelete.from(Cliente.class);
			criteriaDelete.where(criteriaBuilder.equal(root.get("cpf"), cpf));
			em.createQuery(criteriaDelete).executeUpdate();
			em.getTransaction().commit();
		}
		em.close();
	}
	
	public void atualizarCliente(Cliente cliente) {
		EntityManager em = emf.createEntityManager();
		
		Cliente clienteBD = this.pegarClientePorCPF(cliente.getCpf());
		
		if(cliente != null && clienteBD != null) {
			em.getTransaction().begin();
			em.merge(cliente);
			em.getTransaction().commit();
		}
		
		em.close();
	}
	
	public void close() {
		emf.close();
	}

}
