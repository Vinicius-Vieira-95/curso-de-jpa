# crud-clientes-console-app



## Sobre o projeto
Aplicação console Java simples utilizando algumas habilidades minhas de JPA e Criteria no que se remete ao clássico CRUD (create, read, update, delete). A aplicação lhe permite adicioanar clientes únicos identificados pelo seu CPF (neste projeto, tomei o CPF sendo no formato XXXXXXXXXXX), atualizar algum cliente específico, deletar, listar de forma paginada os clientes, e buscar um cliente específico.

## Info
Banco de dados: PostgreSQL<br>
user: postgres<br>
senha: 1234<br>
<br>
você pode estar alterando as configurações conforme suas necessidades o arquivo persistence.xml no diretório META-INF no src/main/resources